import { makeAutoObservable } from 'mobx';

import { IPizza, TSize } from '@/types/pizza.model';

export interface ICart extends IPizza {
    quantity: number;
}

interface ICartStore {
    cart: ICart[];
    total: {
        price: number;
        amount: number;
    };
}

export class CartStore implements ICartStore {
    cart: ICart[] = [];

    total = {
        price: 0,
        amount: 0,
    };

    constructor() {
        makeAutoObservable(this);
    }

    getCartItem(id: number, size: TSize) {
        return this.cart.find(c => c.id === id && c.currentSize === size);
    }

    getTotal = () => {
        this.total = this.cart.reduce(
            (total, c) => {
                const price = c.cost * c.quantity + total.price;
                const amount = c.quantity + total.amount;

                return { price, amount };
            },
            { price: 0, amount: 0 }
        );
    };

    increaseQuantity(id: number, size: TSize) {
        const currentItem = this.getCartItem(id, size);

        if (currentItem.quantity < 99) {
            currentItem.quantity += 1;
        }

        this.getTotal();
    }

    decreaseQuantity(id: number, size: TSize) {
        const currentItem = this.getCartItem(id, size);

        if (currentItem.quantity !== 1) {
            currentItem.quantity -= 1;
        }

        this.getTotal();
    }

    removeFromCart(id: number) {
        this.cart = this.cart.filter(c => c.id !== id);
        this.getTotal();
    }

    addToCart(item: IPizza) {
        const includesItem = this.getCartItem(item.id + item.currentSize, item.currentSize);

        if (!includesItem) {
            this.cart.push({
                ...item,
                id: item.id + item.currentSize,
                quantity: 1,
            });
        } else {
            includesItem.quantity += 1;
        }

        this.getTotal();
    }
}

export const cartStore = new CartStore();
