import { makeAutoObservable } from 'mobx';

import mockPizza from '@/components/Sections/PizzaSection/mockPizza';
import { IPizza, TSize } from '@/types/pizza.model';

interface IPizzaStore {
    pizza: IPizza[];
}

export class PizzaStore implements IPizzaStore {
    pizza = mockPizza;

    constructor() {
        makeAutoObservable(this);
    }

    getPizzaItemIndex(id: number) {
        return this.pizza.find(p => p.id === id);
    }

    changeSize(id: number, newSize: TSize) {
        const pizzaItem = this.getPizzaItemIndex(id);

        pizzaItem.currentSize = newSize;
    }
}

export const pizzaStore = new PizzaStore();
