import '@/components/UI/Input/style.module.sass';

import React from 'react';

import Layout from '@/components/Layout';
import AboutSection from '@/components/Sections/AboutSection';
import DeliverySection from '@/components/Sections/DeliverySection';
import GallerySection from '@/components/Sections/GallerySection';
import MainSection from '@/components/Sections/MainSection';
import PizzaSection from '@/components/Sections/PizzaSection';
import PromotionsSection from '@/components/Sections/PromotionsSection';

const IndexPage = () => (
    <Layout>
        <MainSection />
        <PromotionsSection />
        <PizzaSection />
        <DeliverySection />
        <AboutSection />
        <GallerySection />
    </Layout>
);

export default IndexPage;
