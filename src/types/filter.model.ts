import React from 'react';

import { TPizzaTypes } from '@/types/pizza.model';

export interface IFilter {
    id: number;
    type: TPizzaTypes;
    Icon?: React.FC<React.SVGProps<SVGSVGElement>>;
}
