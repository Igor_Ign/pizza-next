import { FunctionComponent, SVGProps } from 'react';

export interface IPizza {
    id: number;
    imgUrl: string;
    title: string;
    recipe: string;
    sizes: TSize[];
    currentSize: TSize;
    cost: number;
    types: TPizzaTypes[];
    typeIcons: FunctionComponent<SVGProps<SVGSVGElement>>[];
}

export type TSize = 20 | 30 | 40;

// eslint-disable-next-line no-shadow
export enum TPizzaTypes {
    ALL = 'Все',
    PEPPER = 'Острая',
    MEAT = 'Мясная',
    CHEESE = 'Сырная',
    VEGAN = 'Веганская',
}
