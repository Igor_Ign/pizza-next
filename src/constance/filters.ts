import CheeseIcon from '@/components/UI/SVG/filters/CheeseIcon';
import InfinityIcon from '@/components/UI/SVG/filters/InfinityIcon';
import MeatIcon from '@/components/UI/SVG/filters/MeatIcon';
import PepperIcon from '@/components/UI/SVG/filters/PepperIcon';
import VeganIcon from '@/components/UI/SVG/filters/VeganIcon';
import { IFilter } from '@/types/filter.model';
import { TPizzaTypes } from '@/types/pizza.model';

const FILTERS: IFilter[] = [
    {
        id: 0,
        type: TPizzaTypes.ALL,
        Icon: InfinityIcon,
    },
    {
        id: 1,
        type: TPizzaTypes.PEPPER,
        Icon: PepperIcon,
    },
    {
        id: 2,
        type: TPizzaTypes.MEAT,
        Icon: MeatIcon,
    },
    {
        id: 3,
        type: TPizzaTypes.CHEESE,
        Icon: CheeseIcon,
    },
    {
        id: 4,
        type: TPizzaTypes.VEGAN,
        Icon: VeganIcon,
    },
];

export default FILTERS;
