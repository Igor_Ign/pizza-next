const WINDOW_SIZE = {
    mobile: 425,
    tablet: 768,
    desktop: 1024,
};

export default WINDOW_SIZE;
