import { TSize } from '@/types/pizza.model';

const changeSize = (currentSize: TSize) => {
    switch (currentSize) {
        case 20: {
            return '2';
        }
        case 30: {
            return '33';
        }
        case 40: {
            return '66';
        }
        default: {
            return '2';
        }
    }
};

export default changeSize;
