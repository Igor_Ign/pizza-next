import React, { useCallback, useState } from 'react';

// type ReturnedTypes = [boolean, () => void, (e: React.KeyboardEvent<HTMLDivElement>) => void];

const useToggle = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggleHandler = useCallback(() => setIsOpen(!isOpen), [isOpen]);

    const escapeHandler = useCallback((e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === 'escape') {
            setIsOpen(false);
        }
    }, []);

    return {
        isOpen,
        toggleHandler,
        escapeHandler,
        setIsOpen,
    };
};

export default useToggle;
