import { useState } from 'react';

import IRadio from '@/types/radio';

const useCheck = (initial: IRadio) => {
    const [checked, setChecked] = useState(initial);

    const onChangeHandler = (ev: React.ChangeEvent<HTMLInputElement>) =>
        setChecked(ev.target.name as IRadio);

    return { checked, onChangeHandler };
};

export default useCheck;
