import debounce from 'lodash/debounce';
import { useEffect, useState } from 'react';

interface ISize {
    width: number | undefined;
    height: number | undefined;
}

const useWindowSize = (delay: number): ISize => {
    const [windowSize, setWindowSize] = useState<ISize>({
        width: undefined,
        height: undefined,
    });

    useEffect(() => {
        if (typeof window !== undefined) {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }
    }, []);

    useEffect(() => {
        const handleResize = () => {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        };

        const debouncedHandleResize = debounce(handleResize, delay);

        window.addEventListener('resize', debouncedHandleResize);

        return () => window.removeEventListener('resize', debouncedHandleResize);
    }, []);
    return windowSize;
};

export default useWindowSize;
