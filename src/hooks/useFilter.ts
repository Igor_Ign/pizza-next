import { useCallback, useMemo, useState } from 'react';

import { pizzaStore } from '@/domain/MobX/PizzaStore';
import { TPizzaTypes } from '@/types/pizza.model';

const useFilter = () => {
    const { pizza } = pizzaStore;
    const [currentFilter, setCurrentFilter] = useState<TPizzaTypes>(TPizzaTypes.ALL);

    const onChangeFilterHandler = useCallback(
        (newFilter: TPizzaTypes) => setCurrentFilter(newFilter),
        []
    );

    const filteredData = useMemo(() => {
        if (currentFilter === TPizzaTypes.ALL) {
            return pizza;
        }

        return pizza.filter(f => f.types.some(t => t === currentFilter));
    }, [currentFilter, pizza]);

    return {
        filteredData,
        onChangeFilterHandler,
        currentFilter,
    };
};

export default useFilter;
