import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

import mockGallery from '@/components/Sections/GallerySection/mockGallery';

import style from './style.module.sass';

const GallerySection = () => (
    <section className={style['gallery-section']}>
        <div className={style['gallery-section__title']}>Следи за нами в Instagram</div>
        <Link href="#">
            <a className={style['gallery-section__instagram']}>@pizzamenu</a>
        </Link>
        <div className={style['gallery-section__gallery']}>
            {mockGallery.map(item => (
                <div key={item.id} className={style['gallery-section__gallery-item']}>
                    <Image
                        src={item.src}
                        alt={item.alt}
                        width={item.width}
                        height={item.height}
                        layout="responsive"
                    />
                </div>
            ))}
        </div>
    </section>
);

export default GallerySection;
