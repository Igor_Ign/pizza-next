import { observer } from 'mobx-react-lite';
import React, { FC } from 'react';

import { pizzaStore } from '@/domain/MobX/PizzaStore';
import { TSize } from '@/types/pizza.model';
import changeSize from '@/utils/changeSize';

import style from './style.module.sass';

interface PizzaSizeProps {
    sizes: TSize[];
    currentSize: TSize;
    id: number;
}

const PizzaSize: FC<PizzaSizeProps> = ({ currentSize, sizes, id }) => (
    <div className={style['pizza-section__size']}>
        <div className={style['pizza-section__size-title']}>Размер, см:</div>
        <div className={style['pizza-section__size-wrapper']}>
            <div
                className={style['pizza-section__size-pointer']}
                style={{ left: `${changeSize(currentSize)}%` }}
            />
            {sizes?.map(s => (
                <button
                    type="button"
                    key={s}
                    onClick={() => pizzaStore.changeSize(id, s)}
                    className={style['pizza-section__size-diameter']}
                >
                    {s}
                </button>
            ))}
        </div>
    </div>
);
export default observer(PizzaSize);
