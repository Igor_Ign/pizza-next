import clsx from 'clsx';
import { observer } from 'mobx-react-lite';
import React from 'react';

import FILTERS from '@/constance/filters';
import { cartStore } from '@/domain/MobX/CartStore';
import useFilter from '@/hooks/useFilter';
import { IPizza } from '@/types/pizza.model';

import PizzaCard from './PizzaCard';
import style from './style.module.sass';

const PizzaSection = () => {
    const { filteredData, onChangeFilterHandler, currentFilter } = useFilter();

    const addToCardHandler = (pizza: IPizza) => cartStore.addToCart(pizza);

    return (
        <section className={style['pizza-section']}>
            <div className="container">
                <div className={style['pizza-section__title']}>Выберите пиццу</div>
                <div className={style['pizza-section__list']}>
                    {FILTERS.map(({ id, type, Icon }) => (
                        <div
                            role="none"
                            key={id}
                            className={style['pizza-section__item']}
                            onClick={() => onChangeFilterHandler(type)}
                        >
                            <span
                                className={clsx(style['pizza-section__item-text'], {
                                    active: currentFilter === type,
                                })}
                            >
                                {type}
                            </span>
                            <Icon
                                className={clsx(style['pizza-section__item-icon'], {
                                    active: currentFilter === type,
                                })}
                            />
                        </div>
                    ))}
                </div>
                <div className={style['pizza-section__inner']}>
                    {filteredData &&
                        filteredData.map(pizza => (
                            <PizzaCard
                                key={pizza.id}
                                id={pizza.id}
                                cost={pizza.cost}
                                currentSize={pizza.currentSize}
                                imgUrl={pizza.imgUrl}
                                recipe={pizza.recipe}
                                sizes={pizza.sizes}
                                title={pizza.title}
                                typeIcons={pizza.typeIcons}
                                types={pizza.types}
                                addToCartHandler={() => addToCardHandler(pizza)}
                            />
                        ))}
                </div>
            </div>
        </section>
    );
};

export default observer(PizzaSection);
