import cls from 'classnames';
import Image from 'next/image';
import React, { FC } from 'react';

import { IPizza } from '@/types/pizza.model';

import Button from '../../UI/Button';
import PizzaSize from './PizzaSize';
import style from './style.module.sass';

interface PizzaCardProps extends IPizza {
    addToCartHandler: () => void;
}

const PizzaCard: FC<PizzaCardProps> = ({
    cost,
    title,
    recipe,
    sizes,
    imgUrl,
    currentSize,
    typeIcons,
    addToCartHandler,
    id,
}) => (
    <div className={style['pizza-section__card']}>
        <div className={style['pizza-section__card-icon-wrapper']}>
            {typeIcons.map((Icon, index) => (
                <Icon key={index} className={style['pizza-section__card-icon']} />
            ))}
        </div>

        <div className={style['pizza-section__card-wrapper']}>
            <div className={style['pizza-section__card-container']}>
                <div
                    className={cls(
                        {
                            [style['pizza-section__card-img--sm']]: currentSize === 20,
                            [style['pizza-section__card-img--md']]: currentSize === 30,
                            [style['pizza-section__card-img--lg']]: currentSize === 40,
                        },
                        style['pizza-section__card-img']
                    )}
                >
                    <Image
                        src={imgUrl}
                        alt="pizza"
                        width="100%"
                        height="100%"
                        layout="responsive"
                    />
                </div>
            </div>
            <div className={style['pizza-section__card-border']} />
            <div
                className={`${style['pizza-section__card-border']} ${style['pizza-section__card-border--md']}`}
            />
            <div
                className={`${style['pizza-section__card-border']} ${style['pizza-section__card-border--lg']}`}
            />
        </div>
        <div>
            <div className={style['pizza-section__card-title']}>{title}</div>
            <div className={style['pizza-section__card-recipe']}>{recipe}</div>

            <div className={style['pizza-section__card-options']}>
                <PizzaSize sizes={sizes} currentSize={currentSize} id={id} />
                <span className={style['pizza-section__cost']}>от {cost} руб.</span>
                <Button className={style['pizza-section__btn']} onClick={addToCartHandler}>
                    <span className={style['pizza-section__btn-inner_order']}>Заказать</span>
                    <span className={style['pizza-section__btn-inner_cost']}>от {cost} руб.</span>
                </Button>
            </div>
        </div>
    </div>
);

export default PizzaCard;
