import Image from 'next/image';
import React, { FC } from 'react';

import style from './style.module.sass';

interface PromoCardProps {
    src: string;
    alt: string;
    title: string;
    description: string;
}

const PromoCard: FC<PromoCardProps> = ({ description, title, src, alt }) => (
    <div>
        <div className={style['promo-section__img']}>
            <Image src={src} alt={alt} width={416} height={247} />
        </div>
        <div className={style['promo-section__title']}>{title}</div>
        <div className={style['promo-section__description']}>{description}</div>
    </div>
);

export default PromoCard;
