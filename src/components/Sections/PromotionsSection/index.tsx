import dynamic from 'next/dynamic';
import React from 'react';

import WINDOW_SIZE from '@/constance/windowSize';
import useWindowSize from '@/hooks/useWindowSize';

import PromoCard from './PromoCard';
import style from './style.module.sass';

const Slider = dynamic(() => import('./Slider'));

const PromotionsSection = () => {
    const delay = 300;
    const { width } = useWindowSize(delay);

    return (
        <section className={style['promo-section']}>
            <div className="container">
                <div className={style['promo-section__list']}>
                    <div className={style['promo-section__list-inner']}>
                        <PromoCard
                            src="/img/promo/promo1.jpg"
                            alt="promo1"
                            title="Закажи 2 пиццы – 3-я в подарок"
                            description="При заказе 2-х больших пицц – средняя пицца в подарок"
                        />
                        <PromoCard
                            src="/img/promo/promo2.jpg"
                            alt="promo2"
                            title="Напиток в подарок"
                            description="Скидка на заказ от 3 000 рублей + напиток в подарок"
                        />
                        <PromoCard
                            src="/img/promo/promo3.jpg"
                            alt="promo3"
                            title="25% при первом заказе"
                            description="Скидка новым клиентам!"
                        />
                    </div>
                    {width <= WINDOW_SIZE.tablet && <Slider />}
                </div>
            </div>
        </section>
    );
};

export default PromotionsSection;
