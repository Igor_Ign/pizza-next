import clsx from 'clsx';
import React from 'react';
import { Carousel } from 'react-responsive-carousel';

import PromoCard from './PromoCard';
import style from './style.module.sass';

const Slider = () => (
    <Carousel
        showThumbs={false}
        showArrows={false}
        showStatus={false}
        autoPlay
        infiniteLoop
        renderIndicator={(
            clickHandler: (e: React.MouseEvent | React.KeyboardEvent) => void,
            isSelected: boolean
        ) => (
            <div
                role="none"
                onClick={clickHandler}
                className={clsx(style['promo-section__list-dot'], {
                    [style['promo-section__list-dot--active']]: isSelected,
                })}
            />
        )}
    >
        <PromoCard
            src="/img/promo/promo1.jpg"
            alt="promo1"
            title="Закажи 2 пиццы – 3-я в подарок"
            description="При заказе 2-х больших пицц – средняя пицца в подарок"
        />
        <PromoCard
            src="/img/promo/promo2.jpg"
            alt="promo2"
            title="Напиток в подарок"
            description="Скидка на заказ от 3 000 рублей + напиток в подарок"
        />
        <PromoCard
            src="/img/promo/promo3.jpg"
            alt="promo3"
            title="25% при первом заказе"
            description="Скидка новым клиентам!"
        />
    </Carousel>
);

export default Slider;
