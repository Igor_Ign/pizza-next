import React, { FC, SVGProps } from 'react';

import style from './style.module.sass';

interface DeliveryInfoCardProps {
    Icon: FC<SVGProps<SVGElement>>;
    title: string;
    description: string;
}

const DeliveryInfoCard: FC<DeliveryInfoCardProps> = ({ description, title, Icon }) => (
    <div className={style['delivery-section__card']}>
        <Icon />
        <div>
            <div className={style['delivery-section__card-title']}>{title}</div>
            <div className={style['delivery-section__card-description']}>{description}</div>
        </div>
    </div>
);

export default DeliveryInfoCard;
