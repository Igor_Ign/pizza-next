import React from 'react';

import DeliveryIcon from '@/components/UI/SVG/delivery/DeliveryIcon';
import OrderIcon from '@/components/UI/SVG/delivery/OrderIcon';
import PayIcon from '@/components/UI/SVG/delivery/PayIcon';

import DeliveryInfoCard from './DeliveryInfoCard';
import style from './style.module.sass';

const DeliverySection = () => (
    <section className={style['delivery-section']}>
        <div className="container">
            <div className={style['delivery-section__title']}>Доставка и оплата</div>
            <div className={style['delivery-section__wrapper']}>
                <DeliveryInfoCard
                    title="Заказ"
                    description="После оформления заказа мы с вами для уточнения деталей."
                    Icon={OrderIcon}
                />
                <DeliveryInfoCard
                    title="Доставка курьером"
                    description="Мы доставим вашу пиццу горячей. Бесплатная доставка по городу."
                    Icon={DeliveryIcon}
                />
                <DeliveryInfoCard
                    title="Оплата"
                    description="Оплатить можно наличными или картой курьеру. И золотом тоже можно."
                    Icon={PayIcon}
                />
            </div>
        </div>
    </section>
);

export default DeliverySection;
