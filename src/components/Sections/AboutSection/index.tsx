import React from 'react';

import AboutCard from './AboutCard';
import style from './style.module.sass';

const AboutSection = () => (
    <section className={style['about-section']}>
        <div className="container">
            <div className={style['about-section__inner']}>
                <AboutCard
                    title="Изготавливаем пиццу по своим рецептам в лучших традициях"
                    img="/img/about/about01.jpg"
                    alt="about"
                    description="Наша пицца получается сочной, вкусной и главное хрустящей с нежной
                    и аппетитной начинкой, готовим по своим итальянским рецептам"
                />
                <AboutCard
                    title="Используем только свежие ингридиенты"
                    img="/img/about/about02.jpg"
                    alt="about"
                    description="Ежедневно заготавливаем продукты и овощи для наших пицц, соблюдаем все сроки хранения"
                    reverse
                />
                <AboutCard
                    title="Доставка в течение 60 минут или заказ за нас счёт"
                    img="/img/about/about03.jpg"
                    alt="about"
                    description="Все наши курьеры – фанаты серии Need for Speed и призеры гонок
                    World Rally Championship и World Superbike во всех категориях"
                />
            </div>
        </div>
    </section>
);

export default AboutSection;
