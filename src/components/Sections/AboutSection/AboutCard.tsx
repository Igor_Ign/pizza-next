import cls from 'classnames';
import Image from 'next/image';
import React, { FC } from 'react';

import style from './style.module.sass';

interface AboutCardProps {
    img: string;
    alt: string;
    title: string;
    description: string;
    reverse?: boolean;
}

const AboutCard: FC<AboutCardProps> = ({ title, description, img, alt, reverse = false }) => (
    <div className={style['about-section__card']}>
        <div
            className={cls(
                {
                    [style['about-section__card-img--reverse']]: reverse,
                },
                style['about-section__card-img']
            )}
        >
            <Image src={img} alt={alt} width={304} height={304} layout="responsive" />
        </div>
        <div className={style['about-section__card-wrapper']}>
            <div className={style['about-section__card-title']}>{title}</div>
            <div className={style['about-section__card-description']}>{description}</div>
        </div>
    </div>
);

export default AboutCard;
