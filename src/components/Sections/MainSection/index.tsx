import Image from 'next/image';
import React from 'react';

import Button from '../../UI/Button';
import style from './style.module.sass';

const MainSection = () => (
    <section className={style['main-section']}>
        <div className={style['main-section__bg']}>
            <Image
                src="/img/pizza-bg.png"
                alt="pizza-bg"
                width={1007}
                height={630}
                objectFit="contain"
            />
        </div>
        <div className="container">
            <div className={style['main-section__inner']}>
                <div className={style['main-section__title']}>Пицца на заказ</div>
                <div className={style['main-section__description']}>
                    Бесплатная и быстрая доставка за чаc в любое удобное для вас время
                </div>
                <Button className={style['main-section__btn']}>Выбрать пиццу</Button>
            </div>
        </div>
    </section>
);

export default MainSection;
