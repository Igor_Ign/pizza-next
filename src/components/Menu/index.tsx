import clsx from 'clsx';
import React, { FC } from 'react';

import menuItems from './mockMenu';
import style from './style.module.sass';

interface MenuProps {
    scrolled: boolean;
}

const Menu: FC<MenuProps> = ({ scrolled }) => (
    <nav className={style.header__menu}>
        <div className={style['header__menu-list']}>
            {menuItems.map(item => (
                <div
                    key={item}
                    className={clsx(style['header__menu-item'], {
                        [style['header__menu-item--scrolled']]: scrolled,
                    })}
                >
                    {item}
                </div>
            ))}
        </div>
    </nav>
);

export default Menu;
