import clsx from 'clsx';
import { observer } from 'mobx-react-lite';
import Link from 'next/link';
import React, { FC, FunctionComponent, SVGProps } from 'react';

import style from './style.module.sass';

interface InfoProps {
    Icon: FunctionComponent<SVGProps<SVGSVGElement>>;
    isLink?: boolean;
    href?: string;
    title: string;
    subtitle: string;
    onClick?: () => void;
    counter?: number;
    scrolled: boolean;
}

const Info: FC<InfoProps> = ({
    subtitle,
    isLink = false,
    title,
    Icon,
    href,
    onClick,
    counter,
    scrolled,
}) => (
    <div
        className={style.header__info}
        tabIndex={0}
        role="button"
        onKeyDown={onClick}
        onClick={onClick}
    >
        <div className={style['header__info-icon-container']}>
            {!!counter && <div className={style['header__info-counter']}>{counter}</div>}
            {isLink ? (
                <Link href={href}>
                    <a>
                        <Icon width={scrolled ? 32 : 40} height={scrolled ? 32 : 40} />
                    </a>
                </Link>
            ) : (
                <Icon width={scrolled ? 32 : 40} height={scrolled ? 32 : 40} />
            )}
        </div>
        <div className={style['header__info-block']}>
            {isLink ? (
                <Link href={href}>
                    <a
                        className={clsx(
                            style['header__info-title'],
                            style['header__info-title--link'],
                            {
                                [style['header__info-title--link-scrolled']]: scrolled,
                            }
                        )}
                    >
                        {title}
                    </a>
                </Link>
            ) : (
                <div
                    className={clsx(style['header__info-title'], {
                        [style['header__info-title--scrolled']]: scrolled,
                    })}
                >
                    {title}
                </div>
            )}
            <div className={style['header__info-subtitle']}>{subtitle}</div>
        </div>
    </div>
);

export default observer(Info);
