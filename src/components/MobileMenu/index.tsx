import cls from 'classnames';
import React, { FC } from 'react';

import CloseIcon from '@/components/UI/SVG/CloseIcon';
import LogoIcon from '@/components/UI/SVG/LogoIcon';

import menuItems from '../Menu/mockMenu';
import style from './style.module.sass';

interface MobileMenuProps {
    isOpen: boolean;
    onChange: () => void;
}

const MobileMenu: FC<MobileMenuProps> = ({ isOpen, onChange }) => (
    <nav
        className={cls(
            {
                [style.close]: !isOpen,
            },
            style['mobile-menu']
        )}
    >
        <div className={style['mobile-menu__wrapper']}>
            <LogoIcon className={style['mobile-menu__logo']} fill="#fff" />
            <button className={style['mobile-menu__close']} type="button" onClick={onChange}>
                <CloseIcon fill="#fff" />
            </button>
        </div>
        <div className={style['mobile-menu__links']}>
            {menuItems.map(item => (
                <div key={item} className={style['mobile-menu__item']}>
                    <a className={style['mobile-menu__link']} href="#">
                        {item}
                    </a>
                </div>
            ))}
        </div>
        <div className={style['mobile-menu__footer']}>
            <div className={style['mobile-menu__footer-title']}>Заказать по телефону</div>
            <a className={style['mobile-menu__footer-phone']} href="tel:+79184326587">
                +7 (918) 432-65-87
            </a>
            <div className={style['mobile-menu__footer-time']}>Ежедневно с 9:00 до 23:00</div>
            <div className={style['mobile-menu__footer-lang']}>English</div>
        </div>
    </nav>
);

export default MobileMenu;
