import React, { FC } from 'react';

import { cartStore } from '@/domain/MobX/CartStore';
import { TSize } from '@/types/pizza.model';

import MinusIcon from '../UI/SVG/MinusIcon';
import PlusIcon from '../UI/SVG/PlusIcon';
import style from './style.module.sass';

interface CounterProps {
    quantity: number;
    pizzaId: number;
    size: TSize;
}

const Counter: FC<CounterProps> = ({ quantity, pizzaId, size }) => (
    <div className={style.modal__counter}>
        <button
            className={style['modal__counter-btn']}
            type="button"
            onClick={() => cartStore.decreaseQuantity(pizzaId, size)}
        >
            <MinusIcon />
        </button>
        <input className={style['modal__counter-field']} type="text" value={quantity} disabled />
        <button
            className={style['modal__counter-btn']}
            type="button"
            onClick={() => cartStore.increaseQuantity(pizzaId, size)}
        >
            <PlusIcon />
        </button>
    </div>
);

export default Counter;
