import Image from 'next/image';
import React, { FC } from 'react';

import CloseIcon from '@/components/UI/SVG/CloseIcon';
import { cartStore, ICart } from '@/domain/MobX/CartStore';

import Counter from './Counter';
import style from './style.module.sass';

const Item: FC<ICart> = ({ cost, imgUrl, currentSize, title, typeIcons, quantity, id }) => (
    <div className={style.modal__item}>
        <div className={style.modal__pizza}>
            <div className={style['modal__pizza-type']}>
                <div className={style['modal__pizza-icon-wrapper']}>
                    {typeIcons?.map((Icon, index) => (
                        <Icon key={index} className={style['modal__pizza-icon']} />
                    ))}
                </div>
                <Image src={imgUrl} alt="pizza" width="100%" height="100%" layout="responsive" />
            </div>
            <div className={style['modal__pizza-info']}>
                <div className={style['modal__pizza-name']}>{title}</div>
                <div className={style['modal__pizza-size']}>{currentSize}sm</div>
            </div>
        </div>
        <Counter quantity={quantity} pizzaId={id} size={currentSize} />
        <div className={style.modal__cost}>{cost * quantity} руб</div>
        <button
            className={style.modal__remove}
            type="button"
            onClick={() => cartStore.removeFromCart(id)}
        >
            <CloseIcon fill="#A9A9A9" width={24} height={24} />
        </button>
    </div>
);

export default Item;
