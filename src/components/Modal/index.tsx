import clsx from 'clsx';
import { observer } from 'mobx-react-lite';
import React, { Dispatch, FC, SetStateAction, useRef } from 'react';

import Button from '@/components/UI/Button';
import CloseIcon from '@/components/UI/SVG/CloseIcon';
import { cartStore } from '@/domain/MobX/CartStore';
import useCheck from '@/hooks/useCheck';
import useOnClickOutside from '@/hooks/useOnClickOutside';

import Input from '../UI/Input';
import RadioButton from '../UI/RadioButton';
import Item from './Item';
import style from './style.module.sass';

interface ModalProps {
    isOpen: boolean;
    modalToggle: () => void;
    escapeHandler: (e: React.KeyboardEvent<HTMLDivElement>) => void;
    setIsOpen: Dispatch<SetStateAction<boolean>>;
}

const Modal: FC<ModalProps> = ({ isOpen, modalToggle, escapeHandler, setIsOpen }) => {
    const { checked, onChangeHandler } = useCheck('deliver');
    const modalRef = useRef();

    useOnClickOutside(modalRef, () => setIsOpen(false));

    const {
        cart,
        total: { price },
    } = cartStore;

    return (
        <>
            <div
                role="none"
                className={clsx(style.backdrop, { [style['backdrop--show']]: isOpen })}
                onKeyDown={escapeHandler}
            >
                <div
                    ref={modalRef}
                    className={`${style.modal} ${isOpen ? style.open : style.close}`}
                >
                    <div className={style.modal__header}>
                        <div className={style.modal__title}>Ваш заказ</div>
                        <button className={style.modal__close} onClick={modalToggle} type="button">
                            <CloseIcon fill="#000" />
                        </button>
                    </div>

                    {cart.length > 0 ? (
                        <>
                            {cart?.map(cartItem => (
                                <Item
                                    key={`${cartItem.id} - ${cartItem.currentSize}`}
                                    id={cartItem.id}
                                    cost={cartItem.cost}
                                    currentSize={cartItem.currentSize}
                                    imgUrl={cartItem.imgUrl}
                                    quantity={cartItem.quantity}
                                    recipe={cartItem.recipe}
                                    sizes={cartItem.sizes}
                                    title={cartItem.title}
                                    typeIcons={cartItem.typeIcons}
                                    types={cartItem.types}
                                />
                            ))}
                        </>
                    ) : (
                        <div className={style.modal__empty}>Корзина пуста.</div>
                    )}

                    <div className={style.modal__total}>
                        Сумма заказа:{' '}
                        <span className={style['modal__total-inner']}>{price} руб</span>
                    </div>

                    <form className={style.modal__form}>
                        <div className={style['modal__form-title']}>Контакты</div>
                        <div className={style['modal__form-wrapper']}>
                            <div className={style['modal__form-input-wrapper']}>
                                <Input name="name" placeholder="Ваше имя" />
                                <Input name="phone" placeholder="Телефон" />
                            </div>
                            <Input name="address" placeholder="Адрес доставки" />
                        </div>
                        <div className={style['modal__form-title']}>Способ оплаты</div>
                        <div className={style['modal__form-radio-wrapper']}>
                            <RadioButton
                                name="deliver"
                                checked={checked === 'deliver'}
                                title="Оплата наличной или картой курьеру"
                                onChange={onChangeHandler}
                            />
                            <RadioButton
                                name="site"
                                checked={checked === 'site'}
                                title="Оплата картой онлайн на сайт"
                                onChange={onChangeHandler}
                            />
                        </div>
                        <Button
                            className={style['modal__form-submit']}
                            disabled={cart.length === 0}
                        >
                            Оформить заказ
                        </Button>

                        <div className={style['modal__form-politic']}>
                            Нажимая кнопку «Оформить заказ» вы соглашаетесь с политикой
                            конфиденциальности
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default observer(Modal);
