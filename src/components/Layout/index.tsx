import Head from 'next/head';
import React, { FC, ReactNode, useEffect } from 'react';

import Footer from '@/components/Footer';
import Header from '@/components/Header';
import Modal from '@/components/Modal';
import useToggle from '@/hooks/useToggle';

import style from './style.module.sass';

interface LayoutProps {
    children: ReactNode;
}

const Layout: FC<LayoutProps> = ({ children }) => {
    const {
        isOpen: isOpenModal,
        toggleHandler: setIsOpenModal,
        escapeHandler,
        setIsOpen,
    } = useToggle();
    const { isOpen: isOpenMenu, toggleHandler: setIsOpenMenu } = useToggle();

    useEffect(() => {
        document.body.style.overflowY = isOpenModal || isOpenMenu ? 'hidden' : 'auto';
    }, [isOpenModal, isOpenMenu]);

    return (
        <>
            <Head>
                <title>Next starter pack</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Header
                isOpenMenu={isOpenMenu}
                menuToggle={setIsOpenMenu}
                modalToggle={setIsOpenModal}
            />
            <Modal
                isOpen={isOpenModal}
                modalToggle={setIsOpenModal}
                escapeHandler={escapeHandler}
                setIsOpen={setIsOpen}
            />
            <main className={style.main}>{children}</main>
            <Footer />
        </>
    );
};

export default Layout;
