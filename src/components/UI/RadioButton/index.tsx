import React, { FC, useMemo } from 'react';

import style from './style.module.sass';

interface RadioButtonProps
    extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    title: string;
    className?: string;
}

const RadioButton: FC<RadioButtonProps> = ({ title, className, ...props }) => {
    const cls = useMemo(() => {
        if (props.checked) {
            return style['radio-button__wrapper--checked'];
        }

        if (props.disabled) {
            return style['radio-button__wrapper--disabled'];
        }

        return '';
    }, [props.checked, props.disabled]);

    return (
        <label htmlFor={`radio ${props.name}`} className={`${style['radio-button']} ${className}`}>
            <input id={`radio ${props.name}`} type="radio" {...props} />
            <span className={`${style['radio-button__wrapper']} ${cls}`} aria-disabled />
            <span className={style['radio-button__title']}>{title}</span>
        </label>
    );
};

export default RadioButton;
