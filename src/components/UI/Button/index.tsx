import React, { FC, ReactNode } from 'react';

import style from './style.module.sass';

interface ButtonProps
    extends React.DetailedHTMLProps<
        React.ButtonHTMLAttributes<HTMLButtonElement>,
        HTMLButtonElement
    > {
    children: ReactNode;
    className?: string;
}

const Button: FC<ButtonProps> = ({ children, className, ...props }) => (
    <button className={`${style.button} ${className}`} {...props} type="button">
        <span className={style.button__inner}>{children}</span>
    </button>
);

export default Button;
