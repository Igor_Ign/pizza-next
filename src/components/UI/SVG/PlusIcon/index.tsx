import React, { FC, SVGProps } from 'react';

const PlusIcon: FC<SVGProps<SVGSVGElement>> = ({ width, height, fill, className, ...props }) => (
    <svg
        width={!width ? '12' : width}
        height={!height ? '12' : height}
        viewBox="0 0 12 12"
        fill={!fill ? '#fff' : fill}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <rect x="5" width="2" height="12" />
        <rect y="5" width="12" height="2" />
    </svg>
);

export default PlusIcon;
