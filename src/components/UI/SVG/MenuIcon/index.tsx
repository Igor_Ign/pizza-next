import React, { FC, SVGProps } from 'react';

const MenuIcon: FC<SVGProps<SVGSVGElement>> = ({ width, height, fill, className, ...props }) => (
    <svg
        width={!width ? '24' : width}
        height={!height ? '19' : height}
        viewBox="0 0 24 19"
        fill={!fill ? '#000' : fill}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <g id="Menu">
            <g id="Group 64">
                <rect id="Rectangle Copy" width="24" height="3" rx="1.5" />
                <rect id="Rectangle Copy 45" y="8" width="24" height="3" rx="1.5" />
                <rect id="Rectangle Copy 46" y="16" width="19" height="3" rx="1.5" />
            </g>
        </g>
    </svg>
);

export default MenuIcon;
