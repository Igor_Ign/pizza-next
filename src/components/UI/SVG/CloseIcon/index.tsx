import React, { FC, SVGProps } from 'react';

const CloseIcon: FC<SVGProps<SVGSVGElement>> = ({ width, height, fill, className, ...props }) => (
    <svg
        width={!width ? '40' : width}
        height={!height ? '40' : height}
        viewBox="0 0 40 40"
        fill={!fill ? '#fff' : fill}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <rect
            x="12.5752"
            y="10.9541"
            width="24"
            height="3"
            rx="1.5"
            transform="rotate(45 12.5752 10.9541)"
        />
        <rect
            width="24"
            height="3"
            rx="1.5"
            transform="matrix(-0.707107 0.707107 0.707107 0.707107 27.4248 10.9541)"
        />
    </svg>
);

export default CloseIcon;
