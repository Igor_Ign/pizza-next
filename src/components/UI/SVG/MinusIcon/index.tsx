import React, { FC, SVGProps } from 'react';

const MinusIcon: FC<SVGProps<SVGSVGElement>> = ({ width, height, fill, className, ...props }) => (
    <svg
        width={!width ? '12' : width}
        height={!height ? '2' : height}
        viewBox="0 0 12 2"
        fill={!fill ? '#fff' : fill}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <rect width="12" height="2" />
    </svg>
);

export default MinusIcon;
