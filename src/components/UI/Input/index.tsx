import React, { FC, useEffect, useState } from 'react';

import style from './style.module.sass';

interface InputProps
    extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    className?: string;
    name: string;
    placeholder?: string;
}

const Input: FC<InputProps> = ({ className, name, placeholder, ...props }) => {
    const [isFocused, setIsFocused] = useState(false);
    const [value, setValue] = useState('');
    const [error, setError] = useState('');

    const onChangeHandler = (ev: React.ChangeEvent<HTMLInputElement>) =>
        setValue(ev.target.value.trim());

    const onFocusToggleHandler = () => setIsFocused(!isFocused);

    useEffect(() => {
        if (!value && isFocused) {
            setError('Поле обязательно для заполнения!');
        } else {
            setError('');
        }
    }, [value, isFocused]);

    return (
        <div className={style.field}>
            <label className={style.field__label} htmlFor={`input ${name}`}>
                <input
                    id={`input ${name}`}
                    className={`${style.field__input} ${className}`}
                    type="text"
                    value={value}
                    onChange={onChangeHandler}
                    {...props}
                    onFocus={onFocusToggleHandler}
                    onBlur={onFocusToggleHandler}
                />
                <span
                    className={`${style['field__input-placeholder']} ${
                        isFocused || value.length > 0 ? style.focused : ''
                    }`}
                >
                    {placeholder}
                </span>
            </label>
            {error && <small className={style.field__error}>{error}</small>}
        </div>
    );
};

export default Input;
