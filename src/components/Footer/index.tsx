import React from 'react';

import LogoIcon from '@/components/UI/SVG/LogoIcon';

import style from './style.module.sass';

const Footer = () => (
    <footer className={style.footer}>
        <div className="container">
            <div className={style.footer__inner}>
                <div className={style.footer__logo}>
                    <LogoIcon width="100%" height="100%" fill="#fff" />
                </div>
                <div className={style.footer__info}>
                    <a href="tel:+79184236587" className={style.footer__phone}>
                        +7 (918) 432-65-87
                    </a>
                    <div className={style.footer__time}>Ежедневно с 9:00 до 23:00</div>
                </div>
                <a href="#" className={style.footer__politic}>
                    Политика конфиденциальности
                </a>
            </div>
        </div>
    </footer>
);

export default Footer;
