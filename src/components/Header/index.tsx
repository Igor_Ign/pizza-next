import clsx from 'clsx';
import { observer } from 'mobx-react-lite';
import Link from 'next/link';
import React, { FC } from 'react';

import Info from '@/components/Info';
import Menu from '@/components/Menu';
import MobileMenu from '@/components/MobileMenu';
import CartIcon from '@/components/UI/SVG/CartIcon';
import LogoIcon from '@/components/UI/SVG/LogoIcon';
import MenuIcon from '@/components/UI/SVG/MenuIcon';
import PhoneIcon from '@/components/UI/SVG/PhoneIcon';
import { cartStore } from '@/domain/MobX/CartStore';
import useScroll from '@/hooks/useScroll';

import style from './style.module.sass';

interface HeaderProps {
    isOpenMenu?: boolean;
    menuToggle?: () => void;
    modalToggle?: () => void;
}

const Header: FC<HeaderProps> = ({ isOpenMenu, modalToggle, menuToggle }) => {
    const manyItemsSubTitle =
        cartStore.cart.length > 1
            ? `${cartStore.cart[0]?.title} и еще ${cartStore.cart?.length - 1} пиццы`
            : `${cartStore.cart[0]?.title}`;

    const cartSubtitle = cartStore.cart.length !== 0 ? manyItemsSubTitle : 'Корзина пуста';

    const scrolled = useScroll();

    return (
        <header
            className={clsx(style.header, {
                [style['header--scrolled']]: scrolled,
            })}
        >
            <div className="container">
                <div className={style.header__inner}>
                    <Link href="/">
                        <a
                            className={clsx(style['header__logo-container'], {
                                [style['header__logo-container--scrolled']]: scrolled,
                            })}
                        >
                            <LogoIcon className={style.header__logo} />
                        </a>
                    </Link>

                    <Menu scrolled={scrolled} />
                    <MobileMenu isOpen={isOpenMenu} onChange={menuToggle} />
                    <div className={style['header__info-container']}>
                        <Info
                            Icon={PhoneIcon}
                            title="+7 (918) 432-65-87"
                            subtitle="Ежедневно с 9:00 до 23:00"
                            isLink
                            href="tel:+79184326587"
                            scrolled={scrolled}
                        />

                        <Info
                            Icon={CartIcon}
                            title="Ваш заказ"
                            subtitle={cartSubtitle}
                            counter={cartStore.total.amount}
                            onClick={modalToggle}
                            scrolled={scrolled}
                        />

                        <div
                            className={clsx(style.header__lang, {
                                [style['header__lang--scrolled']]: scrolled,
                            })}
                        >
                            En
                        </div>

                        <button
                            className={style.header__hamburger}
                            type="button"
                            onClick={menuToggle}
                        >
                            <MenuIcon />
                        </button>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default observer(Header);
